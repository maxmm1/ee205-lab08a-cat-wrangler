///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file node.hpp
/// @version 1.0
///
/// A generic Node class.  May be used as a base class for a Doubly Linked List
///
/// @author Max Mochizuki <maxmm@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   04/06/2021
///////////////////////////////////////////////////////////////////////////////

#pragma once


class Node {

friend class DoubleLinkedList;

protected:
	Node* next = nullptr;
   Node* prev = nullptr;

public:
	virtual bool operator>(const Node& r);

}; // class Node
