///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.hpp
/// @version 1.0
///
/// Header file for list
///
/// @author Max Mochizuki <maxmm@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   04/06/2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "node.hpp"


class DoubleLinkedList {

   protected:
      Node* head = nullptr;
      Node* tail = nullptr;


   public:
           
      const bool empty() const;
      void push_front( Node* newNode );
      Node* pop_front();
      Node* get_first() const;
      Node* get_next( const Node* currentNode ) const;
      inline unsigned int size() const {
         // Initialize sum and temp Node
         unsigned int sum = 0;
         Node* temp = head;
         
         // Iterate to end of list and increment sum
         while( temp != nullptr ) {
            sum++;
            temp = temp -> next;
         }

         return sum;
      };
      void push_back( Node* newNode );
      Node* pop_back();
      Node* get_last() const;
      Node* get_prev( const Node* currentNode ) const;

      void swap( Node* node1, Node* node2 );
      const bool isSorted() const;  // This function depends on Node's < operator
      void insertionSort();         // This function depends on Node's < operator

};
