///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
///
/// @file list.cpp
/// @version 1.0
///
/// Implementation file for list
///
/// @author Max Mochizuki <maxmm@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   04/06/2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <list>

#include "node.hpp"
#include "list.hpp"


// Return true if the list is empty, false if there’s anything in the list
const bool DoubleLinkedList::empty() const {

   if ( head == nullptr )
      return 1;      // return TRUE
   else
      return 0;      // return FALSE

}


// Add newNode to the front of the list
void DoubleLinkedList::push_front( Node* newNode ) {
   newNode -> prev = nullptr;    // Set start of list
   newNode -> next = head;       // Add newNode before head of list

   // Check if empty list
   if ( head == nullptr ) {
      head = newNode;
      tail = newNode;
   }

   head -> prev = newNode;       // newNode becomes Prev of Head
   head = newNode;               // newNode becomes Head

}


// Remove a node from the front of the list. If the list is already empty, return nullptr
Node* DoubleLinkedList::pop_front() {

   // If empty return nullptr
   if ( head == nullptr ) {
      return nullptr;
   }

   Node* temp = head;        // Hold the address of Head
   head = temp -> next;       // Set the next node as new Head
   head -> prev = nullptr;

   // If one node in the list
   if ( head -> next == nullptr) {
      head = nullptr;
      tail = nullptr;
   }

   return temp;

}


// Return the very first node from the list. Don’t make any changes to the list.
Node* DoubleLinkedList::get_first() const {
   return head;

}


// Return the node immediately following currentNode
Node* DoubleLinkedList::get_next( const Node* currentNode ) const {
   return currentNode -> next;

}


/*
// Return the number of nodes in the list
unsigned int DoubleLinkedList::size() const {

   // Initialize sum and temp Node
   unsigned int sum = 0;
   Node* temp = head;

   // Iterate to end of list and increment sum
   while( temp != nullptr ) {
      sum++;
      temp = temp -> next;
   }

   return sum;
}
*/


// Add newNode to the back of the list
void DoubleLinkedList::push_back( Node* newNode ) {

   newNode -> next = nullptr;    // Set newNode as end of list
   newNode -> prev = tail;       // Add newNode after tail

   // Check if empty list
   if ( head == nullptr ) {
      head = newNode;
      tail = newNode;
   }
  
   tail -> next = newNode;       // newNode comes after tail
   tail = newNode;               // newNode becomes Tail

}


// Remove a node from the back of the list.  If the list is already empty, return nullptr
Node* DoubleLinkedList::pop_back() {

   // If list is empty return nullptr
   if ( head == nullptr ) {
      return nullptr;
   }

   Node* temp = tail;         // Hold address of tail
   tail = temp -> prev;       // Set tail to the previous node in list
   tail -> next = nullptr;

   // If one node in the list, empty the list
   if ( head -> next == nullptr) {
      head = nullptr;
      tail = nullptr;
   }

   return temp;

}


// Return the very last node from the list.  Don’t make any changes to the list
Node* DoubleLinkedList::get_last() const {
   return tail;
}


// Return the node immediately before currentNode
Node* DoubleLinkedList::get_prev( const Node* currentNode ) const {
   return currentNode -> prev;;   
}


// Swaps two nodes in a given list
void DoubleLinkedList::swap( Node* node1, Node* node2 ) {
   
   Node* A = node1 -> next;
   Node* B = node1 -> prev;
   Node* C = node2 -> next;
   Node* D = node2 -> prev;

   // If nodes are the same
   if( node1 == node2 ) {
      std::cout << "Nodes are same" << std::endl;
      return;
   }
   
   // If node1 is head and node2 is tail
   if (( node1 -> prev == nullptr ) || ( node2 -> next == nullptr )) {
     
      node2 -> prev = nullptr;         // Make node2 at beginning of list
      node2 -> next = node1 -> next;   // Switch next
      A -> prev = node2;               // Set prev of second node as node2
      head = node2;                    // Set node2 as head

      node1 -> next = nullptr;         // Make node1 at end of list
      node1 -> prev = D;               // Switch prev
      D -> next = node1;               // Set next of second to last node as node1
      tail = node1;                    // Set node1 as head

      return;
   }

   // If node1 is tail and node2 is head
   if (( node1 -> next == nullptr ) || ( node2 -> prev == nullptr )) {

      node1 -> prev = nullptr;         // Make node1 at beginning of list
      node1 -> next = node2 -> next;   // Switch next
      C -> prev = node1;               // Set prev of second node as node1
      head = node1;                    // Set node1 as head

      node2 -> next = nullptr;         // Make node2 at end of list
      node2 -> prev = B;               // Switch prev
      B -> next = node2;               // Set next of second to last node as node2
      tail = node2;                    // Set node2 as head

      return;
   }

   // If node1 and node2 are adjacent
   if (( node1 -> next == node2 ) ) {
      
      node2 -> prev = B;  
      node2 -> next = node1;
      node1 -> prev = node2;
      node1 -> next = C;

   }

   // If node2 and node1 are adjacent
   if (( node2 -> next == node1 ) ) {

      node1 -> prev = D;
      node1 -> next = node2;
      node2 -> prev = node1;
      node2 -> next = A;

   }

   // If node1 and node2 are not adjacent, and not head or tail
   node1 -> next = C;
   node1 -> prev = D;
   node2 -> next = A;
   node2 -> prev = B;

}


// Determines if a given list is sorted or not
const bool DoubleLinkedList::isSorted() const {
   
   unsigned int count = 0;

   if( count <= 1 ) // If the list is empty or only has one item...
      return true;

   for( Node* i = head ; i -> next != nullptr; i = i -> next ) {
      if( *i > *i -> next )
         return false;
   }

   return true;
}


// Performs an insertion sort on a list
void DoubleLinkedList::insertionSort() {

   // Create empty list for sorted elements
   DoubleLinkedList sorted;

   // If list is empty
   if ( head == nullptr ) {
      std::cout << "List is empty" << std::endl;
      return;
   }

   // Store key value
   

   // Check if each element is smaller than the key

}

